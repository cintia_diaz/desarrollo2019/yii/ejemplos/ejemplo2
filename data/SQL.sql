﻿CREATE DATABASE ejemplo2Yii2Desarrollo;
use ejemplo2Yii2Desarrollo;

CREATE OR REPLACE TABLE catalogo(
  id int auto_increment,
  nombre varchar(100),
  descripcion varchar(100),
  primary key (id)
);

insert into catalogo VALUES 
  (1, 'flor1','Flor del campo'),
  (2, 'flor2','Flor del bosque');
  